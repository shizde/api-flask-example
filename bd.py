cars = [
        {
            'id' : 1,
            'brand' : 'Fiat',
            'model' : 'Marea',
            'year' : 1999
            },
        {
            'id' : 2,
            'brand' : 'Ford',
            'model' : 'Focus',
            'year' : 2005 
            },
        {
            'id' : 3,
            'brand' : 'Volkswagen',
            'model' : 'Gol',
            'year' : 1995
            },
        {
            'id' : 4,
            'brand' : 'Chevrolet',
            'model' : 'Onyx',
            'year' : 2010
            },
         {
            'id' : 5,
            'brand' : 'Hyundai',
            'model' : 'HB20',
            'year' : 2015
            },
        ]
