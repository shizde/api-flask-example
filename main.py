from flask import Flask, make_response, jsonify, request
from .bd import cars

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

@app.route('/cars',methods=['GET'])
def get_cars():
    return make_response(
            jsonify(
                message='Car List',
                data=cars)
            )

@app.route('/cars',methods=['POST'])
def post_cars():
    car = request.json
    cars.append(car)
    return make_response(
            jsonify(
                message='Car added',
                data=car)
            )

if __name__ == "__main__":
    app.run(debug=True)
